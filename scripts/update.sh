#!/bin/bash

# Author: Lucas Marçal Coutinho

# "Imports"

. /etc/os-release

# Officially supported distributions

supportedDistros=(
  debian
  fedora
  manjaro
  ubuntu
)

# Function responsible for show error message and stop script execution

error() {
  ### $1 (first argument): Error message to be displayed before stop the script

  [[ ! -v $1 ]] && echo -ne "\n${lightRedColor}✘${resetColor} $1"
  echo -e "\nExiting...\n"
  exit 1
}

# Finding out the id of the distro or a supported base
# Obs.: If found, it will be stored in a variable called `distroId`

if [[ ${supportedDistros[*]} =~ $ID ]]
then
  echo -e "${lightGreenColor}✔${resetColor} Distribution $ID found!"
  distroId=$ID
else
  echo -e "${lightRedColor}✘${resetColor} Unknown distribution."

  for distroBasisId in ${ID_LIKE[@]}
  do
    if [[ ${supportedDistros[*]} =~ $distroBasisId ]]
    then
      echo -e "${lightGreenColor}✔${resetColor} Distribution basis $distroBasisId found!"
      distroId=$distroBasisId
      break
    fi
  done
  
  [[ ! -v distroId ]] && error "Unable to perform proper operations: without the distro information it is impossible to install any packages."
fi

# Checking if it is running with administrator privileges

[[ ! $EUID -eq 0 ]] && error "This script needs administrator privileges to run properly."

# Updating data from repositories and installed packages via default package manager 

case $distroId in
  "debian" | "ubuntu")
    apt update
    apt upgrade -y
    apt autoremove --purge -y
  ;;
  "fedora")
    dnf check-update --refresh
    dnf upgrade -y
    dnf autoremove -y
  ;;
  "manjaro")
    pamac checkupdates
    pamac upgrade --no-confirm
    pacman -Syyu --noconfirm
    pamac remove --no-confirm --orphans 
  ;;
esac

# Updating installed packages via flatpak package manager 

sudo -u $SUDO_USER flatpak update -y
sudo -u $SUDO_USER flatpak remove --unused

# Updating Rust

rustup update

# Updating installed packages via snap package manager 

snap refresh

# Updating installed packages via npm package manager 

npm update --global

# Updating rust and Cargo

rust update