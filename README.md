![My Linux WorkStation Script](./readme-images/cover.png)

# :shell: Shellter

## :dart: Goal

This is a post-installation script, the purpose of which is to automate the installation of programs, download some files and perform some configurations on the system, in addition to preparing it for changes that cannot be automated.

## :computer: Supported Operating Systems

The script was built using Shell Script with focus on some Linux distros. Officially supported systems are **Debian**, **Fedora**, **Manjaro** and **Ubuntu**. However, if your system is not among the listed OSs, but is based on one of them, the script will identify it and run normally.

## :thinking: What does the script actually do?

As already mentioned, the script's objective is to automate the environment/system preparation, in order to reduce the time spent configuring the distro and installing necessary items after a clean installation.

With that in mind, let it be clear that this project is based on **my preferences **and **configured for my needs**. However, it's open-source, if you don't need everything it will do for you, feel free to use it to create your own :)

So, in a generic way, Shellter will perform the following steps:

1. Identify the distro on which it is running;
2. Create folders (for personal files, system settings and applications);
3. Remove packages (apps that are unwanted or that will be installed newer versions in other ways);
4. Add/configure official and third-party package repositories, configure the default package manager, update data from repositories as well as packages already installed;
5. Install packages pertinent to each distro according to its native package manager;
6. Install packages pertinent to each distro using Flatpak;
7. Install packages pertinent to each distro using Snap;
8. Install packages using NPM;
9. Install programs that are not present in any repository depending on the distro;
10. Install fonts;
11. Configure and install tools to improve the terminal usage experience;
12. Add environment variables;
13. Add environment variables, alias and plugins to the shell.

> At the end of its execution, the script asks if you want to restart the system. It is recommended, but not mandatory. However, some settings made by it will only take effect after restarting.

## :bomb: Running the script

### :skull_and_crossbones: Atention

Again, this script has **my personal settings** like the name and email to use for Git commits. So **don't just download and run this project**, at least change details like this to your settings.

### :robot: It's time

1. Download the _.zip_ file or clone this repository;
2. Open the terminal in the folder you extracted from the compressed file or the clone of that repository;
3. Run the command `chmod +x ./install.sh` to give the permissions to execute the script;
4. Run `sudo su` command to switch to admin user (root);
5. Now run the `./install.sh` file.

> Note that the script really needs root authorization to run, otherwise it will say that it doesn't have permission.
> And using the `sudo` command to try to directly run the script is not feasible without editing the "/etc/sudoers" file.

## :tada: If everything went well...

You now have a system that is almost completely configured and ready for everyday use!

## :memo: License

This project is under the MIT license. See the [LICENSE](LICENSE) for more information.

---

Made with :white_heart: by Lucas Coutinho :wave: [Get in touch!](https://www.linkedin.com/in/lucasmc64/)
