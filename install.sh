#!/bin/bash

# Author: Lucas Marçal Coutinho

# "Imports"

. /etc/os-release

# Global constants

## User home directory

homeDirectory=$(eval echo "~$SUDO_USER")

## Colors and text formatting

boldFont="\e[1m"

lightRedColor="\e[91m"
lightYellowColor="\e[93m"
lightGreenColor="\e[92m"

resetColor="\e[0m"

## Officially supported distributions

supportedDistros=(
  debian
  fedora
  manjaro
  ubuntu
)

## Programs to be installed by the default package manager

commonPackages=(
  ffmpeg                                      # Free Download Manager dependecy
  flatpak                                     # Flatpak support
  gnome-tweaks
  htop
  nodejs                                      # NPM is included
  pandoc                                      # Allow Typora to work with other file extensions
  papirus-icon-theme
  postgresql
  snapd                                       # Snaps support
  spice-vdagent
  vim
  zsh
)

debianPackages=(
  apt-transport-https                         # VSCode dependency
  breeze-cursor-theme                         # Cursor theme
  code
  containerd.io 
  docker-ce 
  docker-ce-cli 
  docker-compose-plugin
  google-chrome-stable
  papirus-folders
  rbenv                                       # ColorLS dependency
  ruby-full                                   # ColorLS dependency
  spice-client-gtk                            # Sharing folders and resources with VMs dependecy
  spice-webdavd
)

fedoraPackages=(
  alien
  android-tools
  autojump-zsh 
  breeze-cursor-theme                         # Cursor theme
  cheat
  clang                                       # Flutter dependency
  cmake                                       # Flutter dependency
  code
  containerd.io 
  dnf-plugins-core
  docker-ce 
  docker-ce-cli 
  docker-compose
  docker-compose-plugin
  gnome-extensions-app
  google-chrome-stable
  gtk3-devel                                  # Flutter dependency
  gtk4-devel
  mesa-libGLU                                 # Flutter dependency
  nautilus-gsconnect
  neovim
  ninja-build                                 # Flutter dependency
  openssl
  pipewire-codec-aptx
  pkg-config                                  # Flutter dependency
  python3-neovim                              # NeoVIM dependency
  rpmrebuild
  ruby                                        # ColorLS dependency
  ruby-devel                                  # ColorLS dependency
  spice-gtk
  spice-webdavd
  steam-devices
  util-linux-user                             # `chsh` is here
  webapp-manager
  webp-pixbuf-loader                          # Webp support
  xz-devel                                    # Flutter dependency
)

ubuntuPackages=(
  ${debianPackages[@]}
)

manjaroPackages=(
  debtap
  eog
  firefox
  google-chrome
  libpamac-flatpak-plugin 
  libpamac-snap-plugin
  manjaro-printer
  nodejs
  npm
  papirus-folders
  ruby                                        # ColorLS dependency
  spice-gtk                                   # Sharing folders and resources with VMs dependecy
  visual-studio-code-bin
  yay
)

## Programs to be installed via Flatpak

flatpakPackages=(
  flathub
  io.bassi.Amberol
  com.google.AndroidStudio
  org.gtk.Gtk3theme.Adwaita-dark

  net.ankiweb.Anki
  org.gnome.gitlab.somas.Apostrophe
  org.gnome.design.AppIconPreview
  com.raggesilver.BlackBox
  org.blender.Blender
  org.gnome.Builder
  com.calibre_ebook.calibre
  com.gitlab.coringao.cavestory-nx
  io.github.lainsce.Colorway
  org.gnome.design.Contrast
  org.gnome.Crosswords
  app.drey.EarTag
  io.github.lainsce.Emulsion
  com.mattjakeman.ExtensionManager
  com.github.finefindus.eyedropper
  com.github.tchx84.Flatseal
  me.hyliu.fluentreader
  de.haeckerfelix.Fragments
  org.gaphor.Gaphor
  io.github.realmazharhusain.GdmSettings
  org.gimp.GIMP
  net.launchpad.gmult
  com.github.GradienceTeam.Gradience
  io.github.hakuneko.HakuNeko
  com.heroicgameslauncher.hgl
  org.gnome.design.IconLibrary
  org.inkscape.Inkscape
  rest.insomnia.Insomnia
  net.cozic.joplin_desktop
  re.sonny.Junction
  org.kde.kdenlive
  io.github.ciromattia.kcc
  org.gnome.design.Lorem
  fr.natron.Natron
  com.obsproject.Studio
  org.onlyoffice.desktopeditors
  org.gnome.design.Palette
  com.getpostman.Postman
  org.libretro.RetroArch
  org.supertuxproject.SuperTux
  net.supertuxkart.SuperTuxKart
  com.spotify.Client
  com.valvesoftware.Steam
  org.gnome.design.SymbolicPreview
  org.telegram.desktop
  org.gnome.design.Typography
  org.videolan.VLC
)

## Programs to be installed via Snap

snapPackages=(
  core
  authy
  bitwarden
  beekeeper-studio
  emote
  gtk-common-themes
  nodemailerapp
  typora
)

## Programs to be installed via NPM

npmPackages=(
  caniuse-cmd
  eslint
  expo-cli
  gitmoji-cli
  hblock
  lighthouse
  live-server
  nodemon
  prettier
  tldr
  vercel
  yarn
)

## Fonts to be installed

fonts=(
  Fira%20Mono
  Fira%20Sans
  Montserrat
  Poppins
  Roboto
  Roboto%20Slab
)

# Generic script funcions

## Function responsible for show error message and stop script execution

error() {
  ### $1 (first argument): Error message to be displayed before stop the script

  [[ ! -v $1 ]] && echo -ne "\n${lightRedColor}✘${resetColor} $1"
  echo -e "\nExiting...\n"
  exit 1
}

## Function responsible for executing all other functions

run() {
  ### $1 (first argument): Function and/or command that will be executed
  ### $2 (second argument): Text to be displayed with the loading animation and the conclusion icon (error or success)
                                                                                                                              # Execution order
  {
    $1 &> /dev/null                                                                                                           # 1
    echo -ne "\r$([[ $? -eq 0 ]] && echo -ne "${lightGreenColor}✔" || echo -ne "${lightRedColor}✘")${resetColor} $2\n"        # 6
  } & {
    pid=$!                                                                                                                    # 2
    i=0                                                                                                                       # 3
    sp="⣾⣽⣻⢿⡿⣟⣯⣷"                                                                                                           # 4

    while [ -d /proc/$pid ]                                                                                                   # 5
    do
      echo -ne "\r${lightYellowColor}${sp:i++%${#sp}:1}${resetColor} $2"
      sleep 0.125
    done
  }
  
  wait
}

## Function responsible for printing titles (basically text in uppercase) throughout the script

printTitle() {
  ### $1 (first argument): Title to be displayed

  echo -e "\n${1^^}\n"
}

# Initializing operations

echo -e "\n${boldFont}$(echo "Shellter" | tr '[:lower:]' '[:upper:]')${resetColor}"

# Checking if it is running with administrator privileges

[[ ! $EUID -eq 0 ]] && error "This script needs administrator privileges to run properly."

# Finding out the id of the distro or a supported base
# Obs.: If found, it will be stored in a variable called `distroId`

printTitle "Analyzing distro"

if [[ ${supportedDistros[*]} =~ $ID ]]
then
  echo -e "${lightGreenColor}✔${resetColor} Distribution $ID found!"
  distroId=$ID
else
  echo -e "${lightRedColor}✘${resetColor} Unknown distribution."

  for distroBasisId in ${ID_LIKE[@]}
  do
    if [[ ${supportedDistros[*]} =~ $distroBasisId ]]
    then
      echo -e "${lightGreenColor}✔${resetColor} Distribution basis $distroBasisId found!"
      distroId=$distroBasisId
      break
    fi
  done
  
  [[ ! -v distroId ]] && error "Unable to perform proper operations: without the distro information it is impossible to install any packages."
fi

# Making pre-configurations before installations to follow

printTitle "Initial configurations"

## Function responsible for create all folders that would be created manually

creatingFolders() {
  sudo -u $SUDO_USER mkdir -p $homeDirectory/Android/Sdk $homeDirectory/Dev $homeDirectory/GitHub $homeDirectory/GitLab $homeDirectory/.themes $homeDirectory/.icons $homeDirectory/.fonts $homeDirectory/.scripts $homeDirectory/.config/colorls $homeDirectory/.vim/pack/themes/start $homeDirectory/.npm-global
}

run creatingFolders "Creating folders"

## Function responsible for remove all unwanted packages or with pre-settings you don't like
## Obs.: I can't use $distroId here, there is no guarantee that it will be necessary to remove them in child distros or even if this causes unforeseen problems

removingUnwantedPackages() {
  case $ID in
    "debian")
      apt purge -y libreoffice libreoffice-common gimp-help-sv firefox-esr aisleriot anthy anthy-common gnome-chess xterm five-or-more four-in-a-row goldendict hdate-applet hitori lightsoff gnome-klotski gnome-mahjongg gnome-mines mlterm-common gnome-nibbles quadrapassel gnome-robots shotwell swell-foop tali gnome-taquin gnome-tetravex evolution gnome-documents gnome-maps rhythmbox gnome-music synaptic xiterm+thai thunderbird iagno gnome-todo-common gnome-sound-recorder transmission-common malcontent
    ;;
    "fedora")
    ;;
    "manjaro")
      pamac remove --no-confirm firefox gthumb
    ;;
    "ubuntu")
    ;;
  esac
}

run removingUnwantedPackages "Removing unwanted packages"

## Function responsible for add/configure official and third-party package repositories, configure the default package manager, update data from repositories as well as packages already installed

settingUpNativePackageStuff() {
  case $distroId in
    "debian" | "ubuntu")
      ### Installing essential packages

      apt install -y wget curl ca-certificates gnupg lsb-release

      ### Adding Google Chrome repository

      if find /etc/apt/sources.list.d -name google-chrome.list | grep . &> /dev/null; [[ ! $? -eq 0 ]]
      then
        sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list' &&
        sudo -u $SUDO_USER wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | apt-key add - 
      fi

      ### Adding Visual Studio Code repository

      if find /etc/apt/sources.list.d -name vscode.list | grep . &> /dev/null; [[ ! $? -eq 0 ]]
      then
        sudo -u $SUDO_USER wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
        install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
        sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
        rm -f packages.microsoft.gpg
      fi

      case $distroId in
        "debian")
          ### Adding Docker repository

          curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
          echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

          ### Adding Node.JS repository

          if find /etc/apt/sources.list.d -name nodesource.list | grep . &> /dev/null; [[ ! $? -eq 0 ]]
          then
            curl -fsSL https://deb.nodesource.com/setup_lts.x | bash -
          fi

          ### Adding Papirus repository

          if find /etc/apt/sources.list.d -name papirus-ppa.list | grep . &> /dev/null; [[ ! $? -eq 0 ]]
          then
            sh -c "echo 'deb http://ppa.launchpad.net/papirus/papirus/ubuntu focal main' > /etc/apt/sources.list.d/papirus-ppa.list"
            apt-get install -y dirmngr
            apt-key adv --recv-keys --keyserver keyserver.ubuntu.com E58A9D36647CAE7F
          fi
        ;;
        "ubuntu")
          ### Adding Docker repository
      
          curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
          echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

          ### Adding Node.JS repository

          if find /etc/apt/sources.list.d -name nodesource.list | grep . &> /dev/null; [[ ! $? -eq 0 ]]
          then
              curl -fsSL https://deb.nodesource.com/setup_lts.x | sudo -E bash -
          fi

          ### Adding Papirus repository

          if find /etc/apt/sources.list.d -name papirus* | grep . &> /dev/null; [[ ! $? -eq 0 ]]
          then
            add-apt-repository -y ppa:papirus/papirus
          fi

          dpkg --configure -a
        ;;
      esac

      [[ $ID == "linuxmint" ]] && rm -f /etc/apt/preferences.d/nosnap.pref
      apt update 
      apt upgrade -y
      apt autoremove --purge -y
    ;;
    "fedora")
      ### Configuring the DNF

      sh -c 'echo -e "fastestmirror=True\nmax_parallel_downloads=10\ndefaultyes=True\nkeepcache=True" >> /etc/dnf/dnf.conf'
      
      ### Installing the dnf-plugins-core package (which provides the commands to manage your DNF repositories)

      dnf install -y dnf-plugins-core

      ### Adding Docker repository

      dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo

      ### Adding Visual Studio Code repository

      if find /etc/yum.repos.d -name vscode.repo | grep . &> /dev/null; [[ ! $? -eq 0 ]]
      then
        rpm --import https://packages.microsoft.com/keys/microsoft.asc
        sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
      fi

      ### Enable Google Chrome repository

      dnf config-manager --set-enabled google-chrome

      ### Enable Fedora’s Third Party repositories

      dnf install -y fedora-workstation-repositories

      ### Enable the RPM Fusion repository for open source software

      dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm

      ### Enable the RPM Fusion repository for non-free software

      dnf install -y https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
      
      ### Enable the WebApp repository

      dnf copr enable perabyte/webapp-manager -y

      ### Show the packages from RPM Fusion repository in the GNOME Software Center

      dnf groupupdate -y core
      dnf check-update --refresh
      dnf upgrade -y
      dnf autoremove -y
    ;;
    "manjaro")
      pamac checkupdates
      pamac upgrade --no-confirm
      pacman -Syyu --noconfirm
      pamac remove --no-confirm --orphans 
    ;;
  esac
}

run settingUpNativePackageStuff "Setting up third-party repositories"

# Performing installations via the default package manager

## Saving the default package manager name JUST to print in the title

case $distroId in
  "debian" | "ubuntu")
    packageManager="apt"
  ;;
  "fedora")
    packageManager="dnf"
  ;;
  "manjaro")
    packageManager="pamac"
  ;;
esac

printTitle "Dealing with $packageManager packages"

## Concentrating all necessary packages in `allPackages`

allPackages=(
  ${commonPackages[@]}
  $(eval "echo \${${distroId}Packages[@]}")
)

## If Gnome Software is installed, plugins necessary for it to manage other types of packages or repositories will be installed

case $distroId in
  "debian" | "ubuntu")
    if dpkg-query -W gnome-software &> /dev/null; [[ $? -eq 0 ]]
    then
      allPackages+=(
        gnome-software-plugin-flatpak
        gnome-software-plugin-snap
      )
    fi
  ;;
  "fedora")
  ;;
  "manjaro")
    if pacman -Qi gnome-software &> /dev/null; [[ $? -eq 0 ]]
    then
      allPackages+=(
        gnome-software-packagekit-plugin 
        archlinux-appstream-data
      )
    fi
  ;;
esac

## Iterating over the `allPackages` array

for package in ${allPackages[@]} 
do
  ### Function responsible for install current package

  installPackage() {
    case $distroId in
      "debian" | "ubuntu")
        apt install -y $package
      ;;
      "fedora")
        dnf install -y $package
      ;;
      "manjaro")
        pamac install --no-confirm $package
      ;;
    esac
  }

  run installPackage "Installing $package"
done

## Install additional multimedia plugins that enable you to play various video and audio types

installFedoraMultimediaLibs() {
  dnf install -y gstreamer1-plugins-{bad-\*,good-\*,base} gstreamer1-plugin-openh264 gstreamer1-libav --exclude=gstreamer1-plugins-bad-free-devel
  dnf install -y lame\* --exclude=lame-devel
  dnf group upgrade -y --with-optional Multimedia
}

[[ $ID == "fedora"  ]] && run installFedoraMultimediaLibs "Installing plugins for playing movies and music"

# Performing installations via the flatpak package manager

printTitle "Dealing with flatpak packages"

## Function responsible for configure the flatpak package manager, update data from repositories as well as packages already installed

settingUpFlatpakPackageManager() {
  flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
  sudo -u $SUDO_USER flatpak update -y
  sudo -u $SUDO_USER flatpak remove --unused -y
}

run settingUpFlatpakPackageManager "Setting up flatpak package manager, updating packages already installed and removing unused dependencies"

## Adding flatpak packages according to the distro

[[ ! $ID == "pop" ]] && flatpakPackages+=(com.system76.Popsicle)
[[ $ID == "debian"  ]] && flatpakPackages+=(org.mozilla.firefox)
[[ $ID == "debian" || $ID == "manjaro" ]] && flatpakPackages+=(org.libreoffice.LibreOffice)
[[ ! $ID == "fedora"  ]] && flatpakPackages+=(org.gnome.Boxes)

## Iterating over the `flatpakPackages` array

for package in ${flatpakPackages[@]} 
do
  ### Function responsible for install current package

  installFlatpakPackage() {
    sudo -u $SUDO_USER flatpak install -y flathub $package
  }

  run installFlatpakPackage "Installing $package"
done

# Performing installations via the snaps package manager

printTitle "Dealing with snap packages"

## Function responsible for configure the snaps package manager, update data from repositories as well as packages already installed

settingUpSnapPackageManager() {
  systemctl start snapd.service
  snap known --remote model model=generic-classic series=16 brand-id=generic > assertion
  snap ack assertion

  [[ $distroId == "kali" ]] && systemctl enable --now snapd apparmor
  [[ $distroId == "arch" || ${ID_LIKE[*]} =~ "arch" ]] && systemctl enable --now snapd.socket

  snap refresh
}

run settingUpSnapPackageManager "Setting up snap package manager and updating packages already installed"

## Iterating over the `snapPackages` array

for package in ${snapPackages[@]}
do
  ### Function responsible for install current package

  installSnapPackage() {
    snap install $package
  }

  run installSnapPackage "Installing $package"
done

# Function responsible for finishing snap configurations

finishingSettingUpSnapPackageManager() {
  [[ $distroId == "arch" || $distroId == "fedora" ]] && ln -s /var/lib/snapd/snap /snap
}

run finishingSettingUpSnapPackageManager "Finishing snap configurations"

# Function responsible for install flutter

installFlutter() {
  snap install flutter --classic
}

run installFlutter "Installing flutter"

# Performing installations via the Node package manager

printTitle "Dealing with NPM packages"


## Function responsible for configure the NPM package manager, update data from repositories as well as packages already installed

settingUpNPMPackageManager() {
  sudo -u $SUDO_USER npm config set prefix '~/.npm-global'
  npm update --global
}

run settingUpNPMPackageManager "Setting up NPM package manager and updating packages already installed"

## Iterating over the `npmPackages` array

for package in ${npmPackages[@]}
do
  ### Function responsible for install current package

  installNPMPackage() {
    if npm list --global $package &> /dev/null; [[ ! $? -eq 0 ]]
    then
      npm install --global $package
    fi
  }

  run installNPMPackage "Installing $package"
done

# Performing installations via another ways

printTitle "Dealing with another packages"

## Function responsible for install Itch.io launcher

installRust () {
  curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y 
}

run installRust "Installing rust"

## Function responsible for install Itch.io launcher

installItch () {
  sudo -u $SUDO_USER wget -O itch-setup https://itch.io/app/download?platform=linux
  chmod +x itch-setup && sudo -u $SUDO_USER ./itch-setup
  rm -f ./itch-setup
}

run installItch "Installing itch-setup"

## Function responsible for install Lunarvim

installLunarvim() {
  bash <(curl -s https://raw.githubusercontent.com/lunarvim/lunarvim/master/utils/installer/install.sh)
}

##[[ $distroId == "fedora" ]] && run installLunarvim "Installing lunarvim"

## Function responsible for install Papirus Folders

installPapirusFolders() {
  sudo -u $SUDO_USER wget -qO- https://git.io/papirus-folders-install | sh
}

[[ ! $distroId == "ubuntu" && ! $distroId == "manjaro" ]] && run installPapirusFolders "Installing papirus-folders"

# Performing installations of fonts

printTitle "Dealing with fonts"

## Function responsible for install Fira Code font

installFiraCodeFont() {
  sudo -u $SUDO_USER wget "$(curl -Ls -o /dev/null -w %{url_effective} https://github.com/ryanoasis/nerd-fonts/releases/latest | sed "s/tag/download/")/FiraCode.zip"
  unzip FiraCode.zip -d ./Fira\sCode
  mv -f ./Fira\sCode $homeDirectory/.fonts
  rm -f ./FiraCode.zip
}

run installFiraCodeFont "Installing Fira Code font"

## Iterating over the `fonts` array

for font in ${fonts[@]}
do
  fontNameWithSpaces=$(echo "${font//"%20"/" "}")

  ### Function responsible for install current font

  installFont() {
    sudo -u $SUDO_USER wget -O "$font".zip https://fonts.google.com/download?family="$font"
    unzip "$font".zip -d ./"$fontNameWithSpaces" 
    mv -f ./"$fontNameWithSpaces" $homeDirectory/.fonts
    rm -f ./"$font".zip
  }

  run installFont "Installing ${fontNameWithSpaces} font"
done

# Additional settings for a beautiful terminal

printTitle "Boosting the terminal"

## Function responsible for change the default shell to ZSH

changingShellToZSH() {
  chsh -s $(which zsh) $SUDO_USER
}

shellPath=$(awk -F: -v u=$SUDO_USER '$1 == u {print $NF}' /etc/passwd)
[[ ! $shellPath == *"zsh"* ]] && run changingShellToZSH "Changing the default shell to ZSH"

## Function responsible for install Oh My ZSH

installOhMyZSH() {
  export CHSH=no
  export RUNZSH=no
  sudo -u $SUDO_USER sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
}

[[ ! -d "$homeDirectory/.oh-my-zsh" ]] && run installOhMyZSH "Installing Oh My ZSH"

## Function responsible for install Zinit

installZinit() {
  sudo -u $SUDO_USER sh -c "$(curl -fsSL https://git.io/zinit-install)"
}

run installZinit "Installing Zinit"

## Function responsible for install ColorLS

installColorLS() {
  gem install colorls
  \cp ./configs/dark_colors.yaml $homeDirectory/.config/colorls/dark_colors.yaml
}

gem list -i colorls &> /dev/null; [[ ! $? -eq 0 ]] && run installColorLS "Installing ColorLS"

# Automating some settings

printTitle "Final touches"

## Function responsible for setting up Android Studio

settingUpAndroidStudio () {
  if cat /etc/group | grep plugdev; [[ ! $? -eq 0 ]]
  then
    groupadd plugdev
  fi

  usermod -aG plugdev $SUDO_USER
}

run settingUpAndroidStudio "Setting up Android Studio"

## Function responsible for setting up Android Studio

settingUpFlutter () {
  sudo -u $SUDO_USER flutter config --no-analytics
  sudo -u $SUDO_USER flutter sdk-path
  sudo -u $SUDO_USER flutter precache
  sudo -u $SUDO_USER flutter config --android-studio-dir /var/lib/flatpak/app/com.google.AndroidStudio/x86_64/stable/active/files/extra/android-studio/
  yes | flutter doctor --android-licenses
}

run settingUpFlutter "Setting up Flutter"


## Function responsible for setting up Git

settingUpGit() {
  sudo -u $SUDO_USER git config --global user.name "Lucas Marçal Coutinho"
  sudo -u $SUDO_USER git config --global user.email coutinho0604@gmail.com
  sudo -u $SUDO_USER git config --global core.editor code
  sudo -u $SUDO_USER git config --global color.ui auto
}

run settingUpGit "Setting up Git"

## Function responsible for setting up hblock

settingUpHblock () {
  hblock
}

run settingUpHblock "Setting up hblock"

## Function responsible for setting up KVM

settingUpKVM () {
  if cat /etc/group | grep kvm; [[ ! $? -eq 0 ]]
  then
    groupadd kvm
  fi

  usermod -aG kvm $SUDO_USER
}

run settingUpKVM "Setting up KVM"

## Function responsible for show "Open in VSCode" in context menu os Nautilus

showVSCodeOnNautilus() {
  sudo -u $SUDO_USER wget -qO- https://raw.githubusercontent.com/harry-cpp/code-nautilus/master/install.sh | sudo -u $SUDO_USER bash
  sed -i "s/\(VSCODENAME = \).*/\1'VSCode'/" "$homeDirectory/.local/share/nautilus-python/extensions/code-nautilus.py"
}

run showVSCodeOnNautilus "Setting up VSCode shortcut in Nautilus"

## Function responsible for setting up Papirus Folders

settingUpPapirusFolders () {
  papirus-folders -C violet --theme Papirus-Dark
}

run settingUpPapirusFolders "Setting up Papirus Folders"

## Function responsible for setting up update script

settingUpUpdateScript () {
  sudo -u $SUDO_USER \cp ./scripts/update.sh $homeDirectory/.scripts
}

run settingUpUpdateScript "Setting up Update script"

## Function responsible for setting up VIM

settingUpVIM () {
  git clone https://github.com/dracula/vim.git $homeDirectory/.vim/pack/themes/start/dracula
  sudo -u $SUDO_USER touch $homeDirectory/.vimrc
  echo -e "packadd! dracula\nsyntax enable\ncolorscheme dracula" | tee -a $homeDirectory/.vimrc
}

[[ ! -d "$homeDirectory/.vim/pack/themes/start/dracula" ]] && run settingUpVIM "Setting up VIM"

## Function responsible for setting up Yarn

settingUpYarn () {
  yarn config set -- --emoji true
}

run settingUpYarn "Setting up Yarn"

## Function responsible for setting up ZSH

settingUpZSH () {
  sudo -u $SUDO_USER \cp ./configs/.env $homeDirectory
  echo -e "\n\n# Custom settings\nsource ~/.env" | tee -a $homeDirectory/.zshrc
}

run settingUpZSH "Setting up ZSH"

# Anyway, the end

printTitle "Done!"

echo "I'm really glad this script helped you :)"
sleep 2
echo "We have finally reached the end of our journey..."
sleep 3
echo "I strongly recommend rebooting the system before actually using it."
sleep 3
echo -e "\n${boldFont}Do you want to restart the system NOW?${resetColor} [y/N]"
sleep 5

read verification

verification=${verification,,}

if [[ $verification == "y" ]] || [[ $verification == "yes" ]]
then 
  reboot
else
  echo -e "\nOkay, I respect your decision."
  echo "Have a nice day :)"
fi

echo -ne "\n"

exit 0
